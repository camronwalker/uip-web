import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddTradeComponent } from './add-trade.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: AddTradeComponent }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
    
  ],
  declarations: [AddTradeComponent]
})
export class AddTradeModule { }
