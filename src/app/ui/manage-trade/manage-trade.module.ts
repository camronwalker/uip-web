import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageTradeComponent } from './manage-trade.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: ManageTradeComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ManageTradeComponent]
})
export class ManageTradeModule { }
