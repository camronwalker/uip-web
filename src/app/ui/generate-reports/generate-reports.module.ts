import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenerateReportsComponent } from './generate-reports.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: GenerateReportsComponent }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GenerateReportsComponent]
})
export class GenerateReportsModule { }