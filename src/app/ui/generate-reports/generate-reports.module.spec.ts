import { GenerateReportsModule } from './generate-reports.module';

describe('GenerateReportsModule', () => {
  let generateReportsModule: GenerateReportsModule;

  beforeEach(() => {
    generateReportsModule = new GenerateReportsModule();
  });

  it('should create an instance', () => {
    expect(generateReportsModule).toBeTruthy();
  });
});
