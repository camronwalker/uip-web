import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page.component';
import { Routes, RouterModule } from '@angular/router';
import { MatCardModule, MatDividerModule } from '@angular/material';


const routes: Routes = [
  { path: '', component: HomePageComponent }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCardModule,
    MatDividerModule,

  ],
  declarations: [HomePageComponent]
})
export class HomePageModule { }
