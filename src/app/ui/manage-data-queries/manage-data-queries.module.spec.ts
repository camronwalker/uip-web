import { ManageDataQueriesModule } from './manage-data-queries.module';

describe('ManageDataQueriesModule', () => {
  let manageDataQueriesModule: ManageDataQueriesModule;

  beforeEach(() => {
    manageDataQueriesModule = new ManageDataQueriesModule();
  });

  it('should create an instance', () => {
    expect(manageDataQueriesModule).toBeTruthy();
  });
});
