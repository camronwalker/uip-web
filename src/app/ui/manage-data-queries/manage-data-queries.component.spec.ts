import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageDataQueriesComponent } from './manage-data-queries.component';

describe('ManageDataQueriesComponent', () => {
  let component: ManageDataQueriesComponent;
  let fixture: ComponentFixture<ManageDataQueriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageDataQueriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageDataQueriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
