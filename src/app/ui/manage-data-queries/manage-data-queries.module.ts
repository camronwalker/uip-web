import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageDataQueriesComponent } from './manage-data-queries.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: ManageDataQueriesComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ManageDataQueriesComponent]
})
export class ManageDataQueriesModule { }
