import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectSetupComponent } from './project-setup.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: ProjectSetupComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProjectSetupComponent]
})
export class ProjectSetupModule { }
