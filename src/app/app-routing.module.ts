import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', loadChildren: './ui/home-page/home-page.module#HomePageModule'},
  { path: 'reports', loadChildren: './ui/generate-reports/generate-reports.module#GenerateReportsModule'},
  { path: 'addtrade', loadChildren: './ui/add-trade/add-trade.module#AddTradeModule'},
  { path: 'dataqueries', loadChildren: './ui/manage-data-queries/manage-data-queries.module#ManageDataQueriesModule'},
  { path: 'managetrade', loadChildren: './ui/manage-trade/manage-trade.module#ManageTradeModule'},
  { path: 'profile', loadChildren: './ui/profile-settings/profile-settings.module#ProfileSettingsModule'},
  { path: 'setup', loadChildren: './ui/project-setup/project-setup.module#ProjectSetupModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true} )],
  exports: [RouterModule]
})
export class AppRoutingModule { }